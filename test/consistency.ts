import {Container} from "../src";

const container = new Container();

// anonymous functions
container.register(() => 5);
container.register(() => 5, []);
container.register(() => 5,
  // @ts-expect-error
  [() => 5]
);
container.register(
  // @ts-expect-error
  (one: number) => one
);
container.register((one: number) => one,
  [() => 5]
);
container.register((one: number) => one,
  [() =>
    // @ts-expect-error
    '5'
  ]
);

const numberToken = container.register(() => 5);
const stringToken = container.register(() => '5');

container.replace(numberToken, numberToken);
container.replace(numberToken,
  // @ts-expect-error
  stringToken
);

container.register((one: number) => one, [numberToken]);
container.register((one: number) => one, [
  // @ts-expect-error
  stringToken
]);

const fooBar = container.register(() => {
  return {
    foo: 'foo',
    bar: 'bar'
  }
});

container.replace(fooBar,
  // @ts-expect-error
  () => {
    return {
      foo: 'foo'
    };
  }
)

// named functions
function five() {
  return 5
}

container.register(five);
container.register(five, []);
container.register(five,
  // @ts-expect-error
  [() => 5]
);

function parameterizedNumber(one: number) {
  return one
}

container.register(
  // @ts-expect-error
  parameterizedNumber
);
container.register(parameterizedNumber, [() => 5]);
container.register(parameterizedNumber,
  [() =>
    // @ts-expect-error
    '5'
  ]
);

const namedNumberToken = container.register(function fiveNumber() {
  return 5;
});

const namedStringToken = container.register(function fiveString() {
  return '5';
});

container.replace(namedNumberToken, namedNumberToken);
container.replace(namedNumberToken,
  // @ts-expect-error
  namedStringToken
);

container.register(parameterizedNumber, [numberToken]);
container.register(parameterizedNumber, [
  // @ts-expect-error
  namedStringToken
]);
